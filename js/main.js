
const BASE_URL = "https://633ec0700dbc3309f3bc569f.mockapi.io"

var tatLoading = function () {
    document.getElementById("loading").style.display = "none";
}
var batLoading = function () {
    document.getElementById("loading").style.display = "flex";
}
// lấy danh sách sinh viên service
var fetchDssvService = function () {
    batLoading();
    axios({
        url: `${BASE_URL}/sv`,
        method: "GET",
    })
        .then(function (response) {
            renderDanhSachSinhVien(response.data);
            tatLoading();
        })
        .catch(function (error) {
            tatLoading();
            console.log("🚀 error", error);

        });

}
// chạy lần đầu khi load trang
fetchDssvService();
// render danh sách sinh viên
var renderDanhSachSinhVien = function (listSv) {

    var contentHTML = "";
    listSv.forEach(function (sv) {
        var dtb = (sv.toan * 1 + sv.hoa * 1 + sv.ly * 1) / 3
        contentHTML += `
<tr>
<td>${sv.ma} </td>
<td>${sv.ten} </td>
<td>${sv.email} </td>
<td>${dtb} </td>
<td>
<button onclick="layThongTinChiTietSv(${sv.ma})" class="btn btn-primary" >sữa</button>
<button onclick="xoaSv(${sv.ma})" class="btn btn-danger" >xóa</button>
 </td>
</tr>
`;
    })

    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
// xóa sv
var xoaSv = function (idSv) {
    batLoading()
    axios({
        url: `${BASE_URL}/sv/${idSv}`,
        method: "DELETE",
    })
        .then(function (res) {
            tatLoading()
            console.log("res", res);

            fetchDssvService();



            Swal.fire('xóa thành công ')
        })
        .catch(function (err) {
            tatLoading()
            Swal.fire('xóa thất bại')
            console.log("err", err)
        })
}

// thêm sinh viên
var themSv = function () {
    var sv = layThongTinTuForm();
    console.log("🚀 sv", sv)

    axios({
        url: `${BASE_URL}/sv`,
        method: "POST",
        data: sv,
    })
        .then(function (res) {
            Swal.fire('thêm thành công')
            fetchDssvService();
        })
        .catch(function (err) {
            Swal.fire('thêm thất bại')
            console.log("err", err)
        })
}

var layThongTinChiTietSv = function (idsv) {
    axios({
        url: `${BASE_URL}/sv/${idsv}`,
        method: 'PUT',
    })
        .then(function (res) {
            console.log(res)
            showThongTinLenForm(res.data)
        })
        .catch(function (err) {
            console.log(err)
        })
}
var capNhapSv = function () {
    var sv = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/sv/${sv.ma}`,
        method: "PUT",
        data: sv,
    })
        .then(function (res) {
            Swal.fire('sữa thành công')
            fetchDssvService();
        })
        .catch(function (err) {
            Swal.fire('sữa thất bại')
            console.log("err", err)
        })
}
