function layThongTinTuForm() {

    // lấy thông tin từ user
    var maSv = document.getElementById("txtMaSV").value.trim(); //  trim remove 2 bên khoảng trắng
    var tenSv = document.getElementById("txtTenSV").value.trim();
    var email = document.getElementById("txtEmail").value.trim();
    var matKhau = document.getElementById("txtPass").value.trim();
    var diemToan = document.getElementById("txtDiemToan").value.trim();
    var diemLy = document.getElementById("txtDiemLy").value.trim();
    var diemHoa = document.getElementById("txtDiemHoa").value.trim();
    // 
    // tạo Object lấy từ form
    var sv = new SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa) // định nghĩa bên file SvmODEL
    return sv;
}

//   khi nào cần truyền tham số vào khi cần đưa dữ liệu từ bên ngoài vào/

function renderDssv(list) {

    // rander danh sách
    var contentHTML = "";
    // contentHTML chuổi chứa các thẻ tr sau này sẽ innerHTML vào thẻ tbody

    for (var i = 0; i < list.length; i++) {

        var currentSv = list[i];
        var contentTr = ` <tr>
        <td> ${currentSv.ma} </td>
        <td> ${currentSv.ten} </td>
        <td> ${currentSv.email} </td>
        <td> ${currentSv.tinhDTB()} </td>
        <td>
        <button onclick="xoaSv('${currentSv.ma}')" class="btn btn-danger" >xóa</button>
        <button onclick="suaSv('${currentSv.ma}')" class="btn btn-primary">sửa</button>

        </td>

        </tr>`;

        contentHTML += contentTr
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML
}

function showThongTinLenForm(sv){
    document.getElementById("txtMaSV").value=sv.ma;
     document.getElementById("txtTenSV").value=sv.ten;
     document.getElementById("txtEmail").value=sv.email;
    document.getElementById("txtPass").value=sv.matKhau;
     document.getElementById("txtDiemToan").value =sv.diemToan;
    document.getElementById("txtDiemLy").value=sv.diemLy ;
     document.getElementById("txtDiemHoa").value=sv.diemHoa;
}